## About 
Continuation of the [bookstore](https://gitlab.com/Yuzyzy88/challenge-topic4) project by implementing a server. All book data is taken from the books.json file and displayed on the page

## Tech
- [Node.js v16.14.2](https://nodejs.org/en/).
- [Express.js 4.17.3](http://expressjs.com/).
- [Template engine ejs](https://github.com/mde/ejs).

## How to use
Run command :
- npm install
- nodemon index.js