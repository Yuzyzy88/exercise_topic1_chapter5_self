// import modul
const express = require('express')
const expressLayouts = require('express-ejs-layouts');
const { loadBooks, findBook } = require('./utils/list');

const app = express();
const port = 8000;

app.use(express.json()) // for parsing application/ json
app.use(express.urlencoded({ extended: true })) // for parsing application/ x-www-form-urlencoded
app.use(expressLayouts);

// template engines
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/book', (req, res) => {
    // to get list books
    const books = loadBooks();
    res.status(200).render('index', {
        layout: 'layouts/main-layout',
        title: 'Home',
        books
    });
});

app.get('/book/:title', (req, res) => {
    const book = findBook(req.params.title);
    res.status(200).render('buy', {
        layout: 'layouts/main-layout',
        title: 'Buy',
        book
    });
    console.log(res.body)
});

app.get('/register', (req, res) => {
    res.status(200).render('register.ejs', {
        layout: 'layouts/main-layout',
        title: 'Register'
    });
});

//if the request page is not recognized
app.use((req, res) => {
    res.status(400).send('404 not found');
})

// to run port 8000
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})