// import module
const fs = require('fs');

// fuction to get all data in books.json
const loadBooks = () => {
    const fileBuffer = fs.readFileSync('data/books.json', 'utf-8');
    const books = JSON.parse(fileBuffer)
    return books;
};

// function find data by title
const findBook = (title) => {
    const books = loadBooks();
    const book = books.find((book) => book.title.toLowerCase() === title.toLowerCase());
    return book;
}

// export loadBooks
module.exports = { loadBooks, findBook };